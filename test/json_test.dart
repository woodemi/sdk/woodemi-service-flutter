import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:woodemi_service/model.dart';
import 'package:woodemi_service/storage.dart';
import 'package:woodemi_service/user.dart';

void main() {
  test('test ClientAgent', () {
    const mock = '''{
  "title": "Smartnote Android Light",
  "appId": 1
}''';
    var clientAgent = ClientAgent.fromJson(jsonDecode(mock));
    expect(clientAgent.appId, 1);
  });

  test('test Environment', () {
    const mock = '''{
  "title": "Smartnote Stage",
  "urlPrefix": "http://39.106.100.225/v2"
}''';
    var environment = Environment.fromJson(jsonDecode(mock));
    expect(environment.urlPrefix, 'http://39.106.100.225/v2');
  });

  test('test CredentialInfo', () {
    const mock = '''{
  "uid": 6000221,
  "accessToken": "89f834faff145aa93bf31cc4354505b2",
  "loginToken": "793ef654b3b7358ed91a0217abe51ef1",
  "accessTokenTTL": 180,
  "loginTokenTTL": 7200
}''';
    var credentialInfo = CredentialInfo.fromJson(jsonDecode(mock));
    expect(credentialInfo.uid, 6000221);
  });

  test('test UserInfo', () {
    const mock = '''{
  "showid": 100010091,
  "nickname": "woodemi-test",
  "avatar": "https://shnote.woodemi.com/app/avatar/avatar.png",
  "gender": 0
}''';
    var userInfo = UserInfo.fromJson(jsonDecode(mock));
    expect(userInfo.showid, 100010091);
  });

  test('test StorageConfig', () {
    const mock = '''{
  "endpoint": "http://oss-cn-beijing.aliyuncs.com",
  "bucket": "smartnote",
  "prefix": "b789/100010091/",
  "quota": 524288000,
  "type": "Private",
  "callback": "/storage/callback"
}''';
    var storageConfig = StorageConfig.fromJson(jsonDecode(mock));
    expect(storageConfig.endpoint, 'http://oss-cn-beijing.aliyuncs.com');
  });

  test('test NoteInfo', () {
    const mock = '''{
  "id": 158778,
  "name": "b78a/100026731/primary.pts",
  "createTime": 1569485267397,
  "size": 38100,
  "state": 1,
  "lastModify": 1569485268404,
  "picName": "b78a/100026731/primary.png",
  "thumbName": "b78a/100026731/primary.png@!thumb",
  "config": "{\\"lastModify\\":1569485268404}",
  "content": "相信美好的事情 即将发生。"
}''';
    var noteInfo = NoteInfo.fromJson(jsonDecode(mock));
    expect(noteInfo.id, 158778);
    expect(noteInfo.config['lastModify'], 1569485268404);
  });

  test('test ShareInfo', () {
    const mock = '''{
  "id": 272,
  "createTime": 1547633124565,
  "lastModify": 1547633129896,
  "shareContent": "我思故我在\\n",
  "like": false,
  "likeCount": 453,
  "commentsCount": 8,
  "paper": {
    "picUrl": "https://shnote.woodemi.com/99fa/100008749/1547600026266.thumb.png",
    "thumbUrl": "https://shnote.woodemi.com/99fa/100008749/1547600026266.thumb.png@!thumb",
    "paperUrl": "https://shnote.woodemi.com/99fa/100008749/1547600026266.pts",
    "paperLastModify": 1547597251484,
    "paperSize": 1101964,
    "paperConfig": "{\\"background\\":\\"https://shnote.woodemi.com/app/notebackground/skin_benbai.png\\",\\"lastModify\\":1547633127929}"
  },
  "author": {
    "gender": 1,
    "uid": 6000823,
    "nickname": "于是",
    "avatar": "http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKibxLG9PEttb8epl7KIHbM7YbiaEehGJNb7nZtZDOrHsQoFqeZ2aohtibxlTre3SIC4ib7ul09qW1bhQ/132"
  }
}''';
    var shareInfo = ShareInfo.fromJson(jsonDecode(mock));
    expect(shareInfo.id, 272);
    expect(shareInfo.paper.paperUrl, 'https://shnote.woodemi.com/99fa/100008749/1547600026266.pts');
  });
}