import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:woodemi_service/ConfigService.dart';
import 'package:woodemi_service/StorageService.dart';
import 'package:woodemi_service/UserService.dart';
import 'package:woodemi_service/WoodemiService.dart';
import 'package:woodemi_service/model.dart';
import 'package:woodemi_service/user.dart';

import 'test_share.dart';
import 'test_sync.dart';

final username = Platform.environment['WOODEMI_USERNAME'];
final password = Platform.environment['WOODEMI_PASSWORD'];

UserInfo userInfo;
CredentialInfo credentialInfo;
String objectPrefix;

void main() {
  WoodemiService.clientAgent = ClientAgent.aSmartnote;
  configService.environment = Environment.sSmartnote;

  final skip = true;

  test('test parseJsonData', () {
    var str = '''<html>
<head><title>504 Gateway Time-out</title></head>
<body bgcolor="white">
<center><h1>504 Gateway Time-out</h1></center>
<hr><center>openresty</center>
</body>
</html>''';
    expect(() => WoodemiService().parseJsonData(str), throwsException);
  });

  test('test ConfigService', () async {
    await configService.fetchConfig();
    expect(configService.userServiceUrl, isNotNull);
    expect(configService.objectStorageUrl, isNotNull);
    expect(configService.mobileUrl, isNotNull);
  });

  group('test UserService', () {
    test('test login', () async {
      userInfo = await userService.login(username, password);
      expect(userInfo.credentialInfo.loginToken, isNotNull);

      var hash = md5.convert(utf8.encode('${userInfo.showid}')).toString().substring(0, 4);
      objectPrefix = '$hash/${userInfo.showid}/';
    });

    test('test refreshAccessToken', () async {
      credentialInfo = await userService.refreshAccessToken(userInfo.credentialInfo.uid, userInfo.credentialInfo.loginToken);
      expect(credentialInfo.accessToken, isNotNull);
    });
  }, skip: skip);

  group('test StorageService', () {
    test('test init', () async {
      storageService.uid = credentialInfo.uid;
      storageService.accessToken = credentialInfo.accessToken;

      await storageService.init();
      expect(storageService.privateConfig, isNotNull);
      expect(storageService.shareConfig, isNotNull);
      expect(storageService.bugConfig, isNotNull);

      expect(storageService.noteFileTypes.points, isNotNull);
      expect(storageService.noteFileTypes.outlinedrawing, isNotNull);
    });

    test('test fetchFederationCredentials', () async {
      var federationCredentials = await storageService.fetchFederationCredentials();
      expect(federationCredentials, isNotNull);
    });

    group('test sync', testSync);

    var skipProduction = configService.environment != Environment.sSmartnote ? 'Skip production' : null;
    group('test share', testShare, skip: skipProduction);
  }, skip: skip);
}