import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:woodemi_service/StorageService.dart';
import 'package:woodemi_service/storage.dart';

void testShare() {
  test('test listShares', () async {
    var shareInfos = await storageService.listShares(0, null, 0, 20);
    expect(shareInfos, isNotEmpty);
  });

  ShareInfo shareInfo;
  test('test createShare & uploadShare', () async {
    var sInfo = await testCreateShare();

    var picContent = await File('test/res/primary.png').readAsBytes();
    var picName = 'primary.png';
    var picInfo = await testUploadSharePic(picName, picContent, sInfo.id);

    var paperName = 'primary.pts';
    var paperContent = await File('test/res/$paperName').readAsBytes();
    var paperText = '相信美好的事情 即将发生。';
    var lastModify = DateTime.now().millisecondsSinceEpoch;
    shareInfo = await testUploadSharePaper(
      paperName,
      paperContent,
      shareId: picInfo.id,
      picName: storageService.shareConfig.prefix + picName, // FIXME
      textContent: paperText,
      lastModify: lastModify,
    );
  });

  test('test getShareInfo', () async {
    var info = await storageService.getShareInfo(shareInfo.id);
    expect(info, shareInfo);
  });
}

Future<ShareInfo> testCreateShare() async {
  var dateTime = DateTime.now();
  var shareContent = '$dateTime';
  var shareInfo = await storageService.createShare(dateTime.millisecondsSinceEpoch, shareContent);

  expect(shareInfo.id, isNotNull);
  // Same seconds, different millis
  expect(shareInfo.createTime ~/ Duration.millisecondsPerSecond, dateTime.millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond);
  expect(shareInfo.shareContent, shareContent);

  return shareInfo;
}

Future<ShareInfo> testUploadSharePic(String picName, Uint8List picData, int shareId) async {
  Map uploadResult = await storageService.uploadObject(storageService.shareConfig, picName, picData, {
    'shareid': '$shareId',
    'filetype': '${storageService.noteFileTypes.outlinedrawing}',
  });
  var shareInfo = ShareInfo.fromJson(uploadResult['share']);

  expect(shareInfo.id, shareId);
  expect(shareInfo.paper.picUrl, endsWith(picName));

  return shareInfo;
}

Future<ShareInfo> testUploadSharePaper(
  String paperName,
  Uint8List paperData, {
  int shareId,
  String picName,
  String textContent,
  int lastModify,
}) async {
  Map uploadResult = await storageService.uploadObject(storageService.shareConfig, paperName, paperData, {
    'shareid': '$shareId',
    'filetype': '${storageService.noteFileTypes.points}',
    'picName': picName,
    'content': textContent,
    'lastModify': '$lastModify',
  });
  var shareInfo = ShareInfo.fromJson(uploadResult['share']);

  expect(shareInfo.id, shareId);
  expect(shareInfo.paper.paperUrl, endsWith(paperName));
  expect(shareInfo.paper.paperContent, textContent);
  expect(shareInfo.paper.paperLastModify, lastModify);

  return shareInfo;
}