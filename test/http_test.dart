import 'dart:convert';
import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:woodemi_service/WoodemiService.dart';

void main() {
  test('test delete', () async {
    var nextInt = Random().nextInt(1024);
    var response = await delete(Uri.parse('https://postman-echo.com/delete'), {
      '$nextInt': nextInt
    });
    var json = jsonDecode(response.body)['json'];
    expect(json['$nextInt'], nextInt);
  });
}