import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:quiver/collection.dart';
import 'package:woodemi_service/StorageService.dart';
import 'package:woodemi_service/storage.dart';

void testSync() {
  test('test listNotes', () async {
    var noteInfos = await storageService.listNotes(0, 0, 20);
    expect(noteInfos, isEmpty);
  });

  NoteInfo noteInfo;
  test('test createNote & uploadNote', () async {
    NoteInfo nInfo = await testCreateNote();

    var picContent = await File('test/res/primary.png').readAsBytes();
    NoteInfo picInfo = await testUploadNotePic('primary.png', picContent, nInfo.id);

    var noteName = 'primary.pts';
    var noteContent = await File('test/res/$noteName').readAsBytes();
    var recognized = '相信美好的事情 即将发生。';
    var lastModify = DateTime.now().millisecondsSinceEpoch;
    var noteConfig = {
      'lastModify': lastModify,
    };
    noteInfo = await testUploadNote(
      noteName,
      noteContent,
      noteId: picInfo.id,
      picName: picInfo.picName,
      recognized: recognized,
      lastModify: lastModify,
      cfg: noteConfig,
    );
  });

  test('test getNoteInfo', () async {
    bool isMilliseconds(int timestamp) {
      var y2k = DateTime(2000);
      var y2kSeconds = y2k.millisecondsSinceEpoch ~/ Duration.millisecondsPerSecond;
      return timestamp < y2kSeconds // 1970 milliseconds from some condition
          || DateTime.fromMillisecondsSinceEpoch(timestamp).isAfter(y2k);
    }

    bool isValid(NoteInfo info) {
      return info.state == NoteInfo.valid
        && isMilliseconds(info.createTime)
        && isMilliseconds(info.lastModify);
    }

    var info = await storageService.getNoteInfo(noteInfo.id);
    expect(isValid(info), isTrue);
    expect(info, noteInfo);
  });

  test('test putNoteConfig', () async {
    var config = {
      ...noteInfo.config,
      'lastModify': DateTime.now().millisecondsSinceEpoch,
    };
    await storageService.putNoteConfig(noteInfo.id, config);

    var info = await storageService.getNoteInfo(noteInfo.id);
    expect(info, isNot(noteInfo));
    expect(mapsEqual(info.config, config), isTrue);
  });

  test('test deleteNote', () async {
    await storageService.deleteNote(noteInfo.id);
  });
}

Future<NoteInfo> testCreateNote() async {
  var createTime = DateTime.now().millisecondsSinceEpoch;
  var nInfo = await storageService.createNote(createTime);

  expect(nInfo.id, isNotNull);
  expect(nInfo.createTime, createTime);
  expect(nInfo.state, NoteInfo.draft);

  return nInfo;
}

Future<NoteInfo> testUploadNotePic(String picName, List<int> picContent, int noteid) async {
  Map uploadResult = await storageService.uploadObject(storageService.privateConfig, picName, picContent, {
    'noteid': '$noteid',
    'filetype': '${storageService.noteFileTypes.outlinedrawing}',
  });
  NoteInfo picInfo = NoteInfo.fromJson(uploadResult['note']);

  expect(picInfo.id, noteid);
  expect(picInfo.picName, endsWith(picName));

  return picInfo;
}

Future<NoteInfo> testUploadNote(
  String name,
  Uint8List content, {
  int noteId,
  String picName,
  String recognized,
  int lastModify,
  Map<String, Object> cfg,
}) async {
  Map noteUploadResult = await storageService.uploadObject(storageService.privateConfig, name, content, {
    'noteid': '$noteId',
    'filetype': '${storageService.noteFileTypes.points}',
    'picName': picName,
    'content': recognized,
    'lastModify': '$lastModify',
    if (cfg != null) 'cfg': jsonEncode(cfg),
  });
  print(noteUploadResult['note']);
  NoteInfo noteInfo = NoteInfo.fromJson(noteUploadResult['note']);

  expect(noteInfo.id, noteId);
  expect(noteInfo.state, NoteInfo.valid);
  expect(noteInfo.name, endsWith(name));
  expect(noteInfo.content, recognized);
  expect(noteInfo.lastModify, lastModify);

  return noteInfo;
}