import 'WoodemiService.dart';
import 'model.dart';

final configService = ConfigService._();

class ConfigService extends WoodemiService {
  ConfigService._();

  Environment environment;

  String _userServiceUrl;
  String get userServiceUrl => _userServiceUrl;

  String _objectStorageUrl;
  String get objectStorageUrl => _objectStorageUrl;

  String _mobileUrl;
  String get mobileUrl => _mobileUrl;

  String _appletsUrl;
  String get appletsUrl => _appletsUrl;

  String _shopUrl;
  String get shopUrl => _shopUrl;

  Future<void> fetchConfig() async {
    Map info = await getJson('${environment.urlPrefix}/config/info');
    var urlConfigs = info['entities'][0];
    _userServiceUrl = urlConfigs['userServiceUrl'];
    _objectStorageUrl = urlConfigs['objectStorageUrl'];
    _mobileUrl = urlConfigs['mobileUrl'];
    _appletsUrl = urlConfigs['appletsUrl'];
    _shopUrl = urlConfigs['shopUrl'];
  }

  Future<bool> fetchReview(String appVer) async {
    Map data = await getJson('${configService.userServiceUrl}/config/iosReview', {
      'appVer': appVer,
    });
    return data['isOnReview'];
  }
}