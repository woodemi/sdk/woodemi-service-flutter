// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CredentialInfo _$CredentialInfoFromJson(Map<String, dynamic> json) {
  return CredentialInfo(
    uid: json['uid'] as int,
    loginToken: json['loginToken'] as String,
    loginTokenTTL: json['loginTokenTTL'] as int,
    accessToken: json['accessToken'] as String,
    accessTokenTTL: json['accessTokenTTL'] as int,
  )
    ..loginTokenUpdated = json['loginTokenUpdated'] as int
    ..accessTokenUpdated = json['accessTokenUpdated'] as int;
}

Map<String, dynamic> _$CredentialInfoToJson(CredentialInfo instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'loginToken': instance.loginToken,
      'loginTokenTTL': instance.loginTokenTTL,
      'loginTokenUpdated': instance.loginTokenUpdated,
      'accessToken': instance.accessToken,
      'accessTokenTTL': instance.accessTokenTTL,
      'accessTokenUpdated': instance.accessTokenUpdated,
    };

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) {
  return UserInfo(
    showid: json['showid'] as int,
    nickname: json['nickname'] as String,
    avatar: json['avatar'] as String,
    gender: json['gender'] as int,
  )..credentialInfo = json['credentialInfo'] == null
      ? null
      : CredentialInfo.fromJson(json['credentialInfo'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'showid': instance.showid,
      'nickname': instance.nickname,
      'avatar': instance.avatar,
      'gender': instance.gender,
      'credentialInfo': instance.credentialInfo,
    };
