import 'ConfigService.dart';
import 'WoodemiService.dart';
import 'user.dart';

final userService = UserService._();

class UserService extends WoodemiService {
  UserService._();

  CredentialInfo parseCredentialInfo(Map response) =>
      CredentialInfo.fromJson(response['entities'][0])
        ..loginTokenUpdated = DateTime.now().millisecondsSinceEpoch
        ..accessTokenUpdated = DateTime.now().millisecondsSinceEpoch;

  UserInfo parseLoginResponse(Map response) =>
      UserInfo.fromJson(response['entities'][0])
        ..credentialInfo = parseCredentialInfo(response);

  //  账号注册登录
  Future<UserInfo> login(String username, String password) async {
    Map response = await postJson('${configService.userServiceUrl}/users/login', {
      'appId': '$appId',
      'username': username,
      'password': password,
    });
    return parseLoginResponse(response);
  }

  Future<UserInfo> authLogin(AuthPlatform authPlatform, String code) async {
    Map response = await postJson('${configService.userServiceUrl}${authPlatform.loginUrlSuffix}', {
      'appId': '$appId',
      'code': code,
    });
    return parseLoginResponse(response);
  }

  Future<void> requestPhoneCode(String phone) async {
    await postJson('${configService.userServiceUrl}/users/mobile/code', {
      'appId': '$appId',
      'phone': phone,
    });
  }

  Future<UserInfo> phoneLogin(String phone, String code) async {
    Map response = await postJson('${configService.userServiceUrl}/users/login/mobile', {
      'appId': '$appId',
      'phone': phone,
      'code': code,
    });
    return parseLoginResponse(response);
  }

  // 账号信息
  Future<BindAccountInfo> getAccountInfo(int uid, String accessToken) async {
    Map response = await getJson(
      '${configService.userServiceUrl}/users/$uid/account?accessToken=$accessToken',
    );
    return BindAccountInfo.fromJson(response);
  }

  //  账号绑定与解绑
  Future<void> bindWeChat(int uid, String accessToken, String code) async {
    await postJson('${configService.userServiceUrl}/users/$uid/wechat', {
      'appId': '$appId',
      'accessToken': accessToken,
      'code': code,
    });
  }

  Future<void> unBindWeChat(int uid, String accessToken) async {
    await deleteJson('${configService.userServiceUrl}/users/$uid/wechat', {
      'accessToken': '$accessToken',
    }, params: {'appId': '$appId'});
  }

  Future<void> bindPhone(int uid, String accessToken, String phone, String code) async {
    await postJson('${configService.userServiceUrl}/users/$uid/mobile', {
      'appId': '$appId',
      'accessToken': accessToken,
      'phone': phone,
      'code': code,
    });
  }

  //  刷新状态
  Future<CredentialInfo> refreshAccessToken(int uid, String loginToken, [String accessToken]) async {
    Map response = await postJson('${configService.userServiceUrl}/auth/accessToken', {
      'appId': '$appId',
      'uid': uid,
      'loginToken': loginToken,
      'accessToken': accessToken,
      'op': 'refresh',
    });
    return parseCredentialInfo(response);
  }

  //  注销账号
  Future<void> deleteAccount(int uid, String accessToken) async {
    await deleteJson('${configService.userServiceUrl}/users/$uid/delete',  {
      'accessToken': '$accessToken',
    }, params: {'appId': '$appId'});
  }
}