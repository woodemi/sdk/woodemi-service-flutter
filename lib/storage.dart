import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:quiver/collection.dart';

part 'storage.g.dart';

@JsonSerializable()
class StorageConfig {
  static const private = 'Private';
  static const share = 'Share';
  static const bug = 'Bug';

  final String type;
  final String endpoint;
  final String bucket;
  final String prefix;
  final String callback;

  StorageConfig({
    this.type,
    this.endpoint,
    this.bucket,
    this.prefix,
    this.callback,
  });

  factory StorageConfig.fromJson(Map<String, dynamic> json) => _$StorageConfigFromJson(json);
  
  Map<String, dynamic> toJson() => _$StorageConfigToJson(this);
}

class NoteFileTypes {
  final Map<String, int> _map;

  NoteFileTypes(this._map);

  int get points => _map['points'];

  int get outlinedrawing => _map['outlinedrawing'];
}

@JsonSerializable()
class NoteInfo {
  static const draft = 0;
  static const valid = 1;
  static const deleted = 2;

  final int id;
  final int createTime;
  final int state;
  final String name;
  final String picName;
  final String content;
  final int lastModify;
  @JsonKey(toJson: _configToJson, fromJson: _configFromJson)
  final Map<String, dynamic> config;

  NoteInfo({
    this.id,
    this.createTime,
    this.state,
    this.name,
    this.picName,
    this.content,
    this.lastModify,
    this.config,
  });

  factory NoteInfo.fromJson(Map<String, dynamic> json) => _$NoteInfoFromJson(json);

  Map<String, dynamic> toJson() => _$NoteInfoToJson(this);

  static String _configToJson(Map<String, dynamic> config) => config != null ? jsonEncode(config) : null;

  static Map<String, Object> _configFromJson(String string) => string != null ? jsonDecode(string) : null;

  @override
  bool operator ==(other) {
    if (other is! NoteInfo) return false;
    NoteInfo o = other;
    return id == o.id &&
        createTime == o.createTime &&
        state == o.state &&
        name == o.name &&
        picName == o.picName &&
        content == o.content &&
        lastModify == o.lastModify &&
        mapsEqual(config, o.config);
  }
}

@JsonSerializable()
class ShareInfo {
  final int id;
  final int createTime;
  final String shareContent;
  final PaperInfo paper;
  final AuthorInfo author;

  ShareInfo({
    this.id,
    this.createTime,
    this.shareContent,
    this.paper,
    this.author
  });

  factory ShareInfo.fromJson(Map<String, dynamic> json) => _$ShareInfoFromJson(json);

  Map<String, dynamic> toJson() => _$ShareInfoToJson(this);

  @override
  bool operator ==(other) {
    if (other is! ShareInfo) return false;
    ShareInfo o = other;
    return id == o.id
        && createTime == o.createTime
        && shareContent == o.shareContent
        && paper == o.paper
        && author == o.author;
  }
}

@JsonSerializable()
class PaperInfo {
  final String paperUrl;
  final String paperContent;
  final String picUrl;
  final String thumbUrl;
  final int paperLastModify;

  PaperInfo({
    this.paperUrl,
    this.paperContent,
    this.picUrl,
    this.thumbUrl,
    this.paperLastModify,
  });

  factory PaperInfo.fromJson(Map<String, dynamic> json) => _$PaperInfoFromJson(json);

  Map<String, dynamic> toJson() => _$PaperInfoToJson(this);

  @override
  bool operator ==(other) {
    if (other is! PaperInfo) return false;
    PaperInfo o = other;
    return picUrl == o.picUrl
        && paperUrl == o.paperUrl
        && thumbUrl == o.thumbUrl
        && paperContent == o.paperContent
        && paperLastModify == o.paperLastModify;
  }
}

@JsonSerializable()
class AuthorInfo {
  final String nickname;
  final String avatar;
  final int gender;

  AuthorInfo({
    this.nickname,
    this.avatar,
    this.gender,
  });

  factory AuthorInfo.fromJson(Map<String, dynamic> json) => _$AuthorInfoFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorInfoToJson(this);

  @override
  bool operator ==(other) {
    if (other is! AuthorInfo) return false;
    AuthorInfo o = other;
    return nickname == o.nickname
        && avatar == o.avatar
        && gender == o.gender;
  }
}