import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

class AuthPlatform {
  static final facebook = AuthPlatform._('/users/login/facebook');
  static final google = AuthPlatform._('/users/login/google');
  static final wechat = AuthPlatform._('/users/login/wechat');

  final String loginUrlSuffix;

  AuthPlatform._(this.loginUrlSuffix);
}

@JsonSerializable()
class CredentialInfo {
  final int uid;
  final String loginToken;
  final int loginTokenTTL; // seconds
  int loginTokenUpdated; // millis
  final String accessToken;
  final int accessTokenTTL; // seconds
  int accessTokenUpdated; // millis

  CredentialInfo({
    this.uid,
    this.loginToken,
    this.loginTokenTTL,
    this.accessToken,
    this.accessTokenTTL,
  });

  factory CredentialInfo.fromJson(Map<String, dynamic> json) => _$CredentialInfoFromJson(json);

  Map<String, dynamic> toJson() => _$CredentialInfoToJson(this);

  int get loginTokenExpire =>
      loginTokenUpdated + loginTokenTTL * Duration.millisecondsPerSecond;

  int get accessTokenExpire =>
      accessTokenUpdated + accessTokenTTL * Duration.millisecondsPerSecond;
}

@JsonSerializable()
class UserInfo {
  final int showid;
  final String nickname;
  final String avatar;
  final int gender;
  CredentialInfo credentialInfo;

  UserInfo({
    this.showid,
    this.nickname,
    this.avatar,
    this.gender,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) => _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}

class BindAccountInfo {
  final String mobile;
  final String wechatNickName;

  BindAccountInfo({
    this.mobile,
    this.wechatNickName,
  });

  factory BindAccountInfo.fromJson(Map<String, dynamic> json) =>
      BindAccountInfo(
        mobile: json['mobile'] as String,
        wechatNickName: json['wechatNickName'] as String,
      );
}