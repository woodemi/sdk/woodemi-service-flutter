// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'storage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StorageConfig _$StorageConfigFromJson(Map<String, dynamic> json) {
  return StorageConfig(
    type: json['type'] as String,
    endpoint: json['endpoint'] as String,
    bucket: json['bucket'] as String,
    prefix: json['prefix'] as String,
    callback: json['callback'] as String,
  );
}

Map<String, dynamic> _$StorageConfigToJson(StorageConfig instance) =>
    <String, dynamic>{
      'type': instance.type,
      'endpoint': instance.endpoint,
      'bucket': instance.bucket,
      'prefix': instance.prefix,
      'callback': instance.callback,
    };

NoteInfo _$NoteInfoFromJson(Map<String, dynamic> json) {
  return NoteInfo(
    id: json['id'] as int,
    createTime: json['createTime'] as int,
    state: json['state'] as int,
    name: json['name'] as String,
    picName: json['picName'] as String,
    content: json['content'] as String,
    lastModify: json['lastModify'] as int,
    config: NoteInfo._configFromJson(json['config'] as String),
  );
}

Map<String, dynamic> _$NoteInfoToJson(NoteInfo instance) => <String, dynamic>{
      'id': instance.id,
      'createTime': instance.createTime,
      'state': instance.state,
      'name': instance.name,
      'picName': instance.picName,
      'content': instance.content,
      'lastModify': instance.lastModify,
      'config': NoteInfo._configToJson(instance.config),
    };

ShareInfo _$ShareInfoFromJson(Map<String, dynamic> json) {
  return ShareInfo(
    id: json['id'] as int,
    createTime: json['createTime'] as int,
    shareContent: json['shareContent'] as String,
    paper: json['paper'] == null
        ? null
        : PaperInfo.fromJson(json['paper'] as Map<String, dynamic>),
    author: json['author'] == null
        ? null
        : AuthorInfo.fromJson(json['author'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ShareInfoToJson(ShareInfo instance) => <String, dynamic>{
      'id': instance.id,
      'createTime': instance.createTime,
      'shareContent': instance.shareContent,
      'paper': instance.paper,
      'author': instance.author,
    };

PaperInfo _$PaperInfoFromJson(Map<String, dynamic> json) {
  return PaperInfo(
    paperUrl: json['paperUrl'] as String,
    paperContent: json['paperContent'] as String,
    picUrl: json['picUrl'] as String,
    thumbUrl: json['thumbUrl'] as String,
    paperLastModify: json['paperLastModify'] as int,
  );
}

Map<String, dynamic> _$PaperInfoToJson(PaperInfo instance) => <String, dynamic>{
      'paperUrl': instance.paperUrl,
      'paperContent': instance.paperContent,
      'picUrl': instance.picUrl,
      'thumbUrl': instance.thumbUrl,
      'paperLastModify': instance.paperLastModify,
    };

AuthorInfo _$AuthorInfoFromJson(Map<String, dynamic> json) {
  return AuthorInfo(
    nickname: json['nickname'] as String,
    avatar: json['avatar'] as String,
    gender: json['gender'] as int,
  );
}

Map<String, dynamic> _$AuthorInfoToJson(AuthorInfo instance) =>
    <String, dynamic>{
      'nickname': instance.nickname,
      'avatar': instance.avatar,
      'gender': instance.gender,
    };
