import 'dart:typed_data';

import 'package:aliyun_oss/OSSClient.dart';
import 'package:aliyun_oss/common.dart';
import 'package:woodemi_service/storage.dart';

import 'ConfigService.dart';
import 'WoodemiService.dart';

final storageService = StorageService._();

class StorageService extends WoodemiService with FederationCredentialProvider {
  int uid;
  String accessToken;

  StorageService._();

  List<StorageConfig> _ossConfigs;
  StorageConfig get privateConfig => _ossConfigs.firstWhere((c) => c.type == StorageConfig.private);
  StorageConfig get shareConfig => _ossConfigs.firstWhere((c) => c.type == StorageConfig.share);
  StorageConfig get bugConfig => _ossConfigs.firstWhere((c) => c.type == StorageConfig.bug);

  NoteFileTypes _noteFileTypes;
  NoteFileTypes get noteFileTypes => _noteFileTypes;

  Future<void> init() async {
    Map data = await getJson('${configService.objectStorageUrl}/storage/users/$uid/config', {
      'accessToken': accessToken,
    });
    _ossConfigs = (data['oosConfigs'] as List).map((m) => StorageConfig.fromJson(m)).toList();
    _noteFileTypes = NoteFileTypes((data['noteFileTypes'] as Map).cast());
  }

  @override
  Future<FederationCredentials> fetchFederationCredentials() async {
    Map data = await postJson('${configService.objectStorageUrl}/storage/auth', {
      'appId': appId,
      'accessToken': accessToken,
      'appTimestamp': DateTime.now().millisecondsSinceEpoch,
    });
    return FederationCredentials.fromMap(data['auth']);
  }

  Future<List<NoteInfo>> listNotes(int startTime, int startIndex, int recordNum) async {
    Map data = await getJson('${configService.objectStorageUrl}/storage/users/$uid/notes', {
      'accessToken': accessToken,
      'starttime': '$startTime',
      'startindex': '$startIndex',
      'recordnum': '$recordNum',
    });
    return (data['notes'] as List).map((m) => NoteInfo.fromJson(m)).toList();
  }

  Future<NoteInfo> getNoteInfo(int noteId) async {
    Map data = await getJson('${configService.objectStorageUrl}/storage/users/$uid/notes/$noteId', {
      'accessToken': accessToken,
    });
    return NoteInfo.fromJson(data['note']);
  }

  Future<NoteInfo> createNote(int createTime) async {
    Map data = await postJson('${configService.objectStorageUrl}/storage/users/$uid/notes', {
      'accessToken': accessToken,
      'createTime': createTime,
    });
    return NoteInfo.fromJson(data['note']);
  }

  Future<Object> uploadObject(StorageConfig config, String name, Uint8List content, Map<String, String> vars) async {
    var callbackUrl = configService.objectStorageUrl + config.callback;
    var callbackRequest = await _buildUploadCallbackRequest(callbackUrl, vars);

    var bodyString = await OSSClient(config.endpoint, this).putObject(
      bucket: config.bucket,
      objectKey: config.prefix + name,
      content: content,
      contentType: 'application/octet-stream',
      callback: callbackRequest,
    );

    return parseJsonData(bodyString);
  }

  Future<OSSCallbackRequest> _buildUploadCallbackRequest(String url, Map<String, String> extVars) async {
    return OSSCallbackRequest.build(
      url,
      systemParams: {
        'filename': OSSCallbackRequest.VAR_OBJECT,
        'etag': OSSCallbackRequest.VAR_ETAG,
        'size': OSSCallbackRequest.VAR_SIZE,
      },
      customVars: {
        'appid': '$appId',
        'uid': '$uid',
        ...extVars,
        'phone': WoodemiService.operationSystem,
        'system': WoodemiService.operatingSystemVersion,
      },
    );
  }

  Future<void> putNoteConfig(int noteId, Map<String, Object> config) async {
    await putJson('${configService.objectStorageUrl}/storage/users/$uid/notes/$noteId', {
      'accessToken': accessToken,
      ...config,
    });
  }

  Future<void> deleteNote(int noteId) async {
    await deleteJson('${configService.objectStorageUrl}/storage/users/$uid/notes/$noteId', {
      'accessToken': accessToken,
      'deletetime': '${DateTime.now().millisecondsSinceEpoch}',
    });
  }

  Future<List<ShareInfo>> listShares(int starttime, int endtime, int startindex, int recordnum) async {
    Map data = await getJson('${configService.objectStorageUrl}/share/shares/new', {
      if (uid != null) 'uid': '$uid',
      'starttime': '$starttime',
      if (endtime != null) 'endtime': '$endtime',
      'startindex': '$startindex',
      'recordnum': '$recordnum',
    });
    return (data['shares'] as List).map((m) => ShareInfo.fromJson(m)).toList();
  }

  Future<ShareInfo> createShare(int createTime, String content) async {
    Map data = await postJson('${configService.objectStorageUrl}/share/$uid/createShare', {
      'accessToken': accessToken,
      'createTime': createTime,
      'content': content
    });
    return ShareInfo.fromJson(data['share']);
  }

  Future<ShareInfo> getShareInfo(int shareId) async {
    Map data = await getJson('${configService.objectStorageUrl}/share/share/$shareId');
    return ShareInfo.fromJson(data['share']);
  }
}
