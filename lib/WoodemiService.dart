import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import 'common.dart';
import 'model.dart';

class WoodemiService {
  static ClientAgent clientAgent;
  static String operationSystem = 'Unknown';
  static String operatingSystemVersion = 'Unknown';

  final _logger = Logger("WoodemiService");

  int get appId => clientAgent.appId;

  Object parseJsonData(String bodyString) {
    try {
      _logger.info('response = $bodyString');
      var json = jsonDecode(bodyString);
      var response = WoodemiResponse.fromMap(json);
      if (response.status.statuscode != WoodemiStatus.success) {
        throw WoodemiException(response.status);
      }
      return response.data;
    } on WoodemiException catch (e) {
      rethrow;
    } catch (e) {
      // In case of jsonDecode error
      _logger.severe('parseJsonData $bodyString');
      throw NetException(0, bodyString);
    }
  }

  Future<Object> getJson(String url, [Map<String, String> params]) async {
    _logger.info('---------------------getJson---------------------');
    _logger.info('url = ${url}');
    _logger.info('params = ${params}');
    var uri = Uri.parse(url).replace(queryParameters: params).toString();
    var response = await http.get(uri);
    return parseJsonData(response.body);
  }

  /// Default to [ContentType.json]
  Future<Object> postJson(String url, Map<String, Object> params) async {
    _logger.info('---------------------postJson---------------------');
    _logger.info('url = ${url}');
    _logger.info('params = ${params}');
    var response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: jsonEncode(params),
    );
    return parseJsonData(response.body);
  }

  /// Default to [ContentType.json]
  Future<Object> putJson(String url, Map<String, Object> params) async {
    _logger.info('---------------------putJson---------------------');
    _logger.info('url = ${url}');
    _logger.info('params = ${params}');
    var response = await http.put(
      url,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: jsonEncode(params),
    );
    return parseJsonData(response.body);
  }

  Future<Object> deleteJson(String url, Map<String, String> queryParameters, {Map<String, Object> params}) async {
    _logger.info('---------------------deleteJson---------------------');
    _logger.info('url = ${url}');
    _logger.info('params = ${queryParameters}');
    var uri = Uri.parse(url).replace(queryParameters: queryParameters);
    var response = await delete(uri, params);
    return parseJsonData(response.body);
  }
}

// FIXME https://github.com/dart-lang/http/issues/206
@visibleForTesting
Future<http.Response> delete(Uri uri, Map<String, Object> params) async {
  var client = http.Client();
  try {
    var request = http.Request('DELETE', uri);
    if (params != null) {
      request.headers.addAll({
        'Content-Type': 'application/json; charset=utf-8',
      });
      request.body = jsonEncode(params);
    }
    var streamedResponse = await client.send(request);
    return await http.Response.fromStream(streamedResponse);
  } finally {
    client.close();
  }
}